<?php

namespace Flyshot\Dotenv;

use Symfony\Component\Dotenv\Dotenv as SfDotenv;

class Dotenv
{
    private SfDotenv $dotenv;

    public function __construct()
    {
        $this->dotenv = new SfDotenv();
    }

    public function loadEnv(string $path, string $varName = 'APP_ENV', string $defaultEnv = 'dev', array $testEnvs = ['test']): void
    {
        if (file_exists($path) || !file_exists($p = "$path.dist")) {
            $this->dotenv->overload($path);
        } else {
            $this->dotenv->overload($p);
        }

        if (null === $env = $_ENV[$varName] ?? $_SERVER[$varName] ?? null) {
            $this->dotenv->populate([$varName => $env = $defaultEnv]);
        }

        if (!\in_array($env, $testEnvs, true) && file_exists($p = "$path.local")) {
            $this->dotenv->overload($p);
            $env = $_ENV[$varName] ?? $_SERVER[$varName] ?? $env;
        }

        if ('local' === $env) {
            return;
        }

        if (file_exists($p = "$path.$env")) {
            $this->dotenv->overload($p);
        }

        if (file_exists($p = "$path.$env.local")) {
            $this->dotenv->overload($p);
        }
    }
}
