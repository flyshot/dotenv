Installation
-----

Use [Composer](https://getcomposer.org/) to add library to project:

```bash
composer req flyshot/dotenv
```
